If you for whatever reason want to help me, I'd appreciate contacting me first. 
This project is so small in scope everything should be able to be taken care 
of individually.
If you're just looking around and haven't decided yet, you can see the main 
issues under Issues.
I would also highly appreciate any advice of any nature for writing better code.
I'm surprised someone actually read this.