#include "MMatrix.hpp"

#include <iostream>

using namespace std;

template<class T, class U>
void test(const T &a, const U &b)
{
    if (a != b)
    {
        cout << "Test Failed\nFollowing two values do not match\n" << a << '\n' << b << endl;
    }
}

int main()
{
    cout << "Testing... \nInitialization" << endl;

    MMatrix<2, 2> mat1_1=
    {
        {1, 2},
        {5, 6}
    };
    test(mat1_1, MMatrix<2, 2>{{1,2},{5,6}});

    MMatrix<100, 100> mat1_2;
    test(mat1_2, MMatrix<100, 100>());

    MMatrix<0, 0> mat1_3;
    test(mat1_3, MMatrix<0, 0>());

    cout << "at()" << endl;

    mat1_1.at(0,0) = 6;
    test(mat1_1, MMatrix<2, 2>{{6,2},{5,6}});

    mat1_1.at(1,1) = 9;
    test(mat1_1, MMatrix<2, 2>{{6,2},{5,9}});

    cout << "read()" << endl;

    test(mat1_1.read(0,1), 2);

    test(mat1_2.read(99,99), 0);

    constexpr MMatrix<1, 3> mat2_1 =
    {
        {6, 5, 4}
    };

    constexpr MMatrix<1, 3> mat2_2 =
    {
        {-8, 190, 0.003}
    };

    constexpr MMatrix<3, 1> mat2_3 =
    {
        {4},
        {3},
        {2}
    };

    cout << "Addition" << endl;

    test(mat2_1+mat2_2, MMatrix<1, 3>{{-2, 195, 4.003}});

    cout << "Subtraction" << endl;

    test(mat2_1-mat2_2, MMatrix<1, 3>{{14, -185, 3.997}});

    cout << "Scalar multiplication" << endl;

    test(mat2_1*(-2), MMatrix<1, 3>{{-12, -10, -8}});

    cout << "Scalar division" << endl;

    test(mat2_1/0.2, MMatrix<1, 3>{{30, 25, 20}});

    cout << "Matrix multiplication" << endl;

    test(mat2_1*mat2_3, MMatrix<1, 1>{{47}});
    test(mat2_3*mat2_1, MMatrix<3, 3>{{24, 20, 16}, {18, 15, 12}, {12, 10, 8}});

    MMatrix<4, 4> mat3_1=
    {
        {1, 2, 3, 4},
        {-6, 90, 3, 12},
        {0, 3, 7, -5},
        {11, -55, 1, 1}
    };

    cout << "extract()" << endl;

    test(mat3_1.extract<3, 2>(1, 1), MMatrix<3, 2>{{90, 3}, {3, 7}, {-55, 1}});

    cout << "insert()" << endl;

    test(mat3_1.insert(2, 2, MMatrix<1, 2>{{1, 2}}), MMatrix<4, 4>{{1, 2, 3, 4}, {-6, 90, 3, 12}, {0, 3, 1, 2}, {11, -55, 1, 1}});

    cout << "transpose()" << endl;

    test(mat3_1.transpose(), MMatrix<4, 4>{{1, -6, 0, 11}, {2, 90, 3, -55}, {3, 3, 7, 1}, {4, 12, -5, 1}});
    test(mat2_3.transpose(), MMatrix<1, 3>{{4, 3, 2}});

    cout << "get_rre()" << endl;

    test(MMatrix<1, 1>{{8}}.get_rre(), MMatrix<1, 1>{{1}});
    test(mat3_1.get_rre(), MMatrix<4, 4>{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});

    MMatrix<3, 4> mat4_1 =
    {
        {5, -8, 6, 7},
        {-1, 5, 4, 3},
        {1, 3, 2, 10}
    };

    test(mat4_1.get_rre(), MMatrix<3, 4>{{1, 0, 0, 5.01886792}, {0, 1, 0, 1.94339622}, {0, 0, 1, -0.42452830}});

    MMatrix<2, 3> mat4_2 =
    {
        {0, 3, 2},
        {5, 0, 6}
    };

    test(mat4_2.get_rre(), MMatrix<2, 3>{{1, 0, 1.2}, {0, 1, 0.66666667}});

    MMatrix<3, 2> mat4_3 =
    {
        {1, 4},
        {0, 0},
        {5, 3}
    };

    test(mat4_3.get_rre(), MMatrix<3, 2>{{1, 0}, {0, 1}, {0, 0}});

    MMatrix<2, 2> mat4_4 =
    {
        {0, 0},
        {0, 0}
    };

    test(mat4_4.get_rre(), MMatrix<2, 2>{{0, 0}, {0, 0}});

    cout << "get_rank()" << endl;

    test(mat4_4.get_rank(), static_cast<std::size_t>(0));
    test(mat4_3.get_rank(), static_cast<std::size_t>(2));
    test(mat4_2.get_rank(), static_cast<std::size_t>(2));
    test(mat4_1.get_rank(), static_cast<std::size_t>(3));

    cout << "get_det()" << endl;

    test(mat4_4.get_det(), 0);
    test(mat3_1.get_det(), -20280);

    constexpr MMatrix<3, 3> mat5_1 =
    {
        {1, 34, 0},
        {-6, 3, 2},
        {0, 0, -8}
    };

    test(mat5_1.get_det(), -1656);

    cout << "get_inv()" << endl;

    constexpr MMatrix<3, 3> mat5_2 = mat5_1.get_inv();

    test(mat5_2, MMatrix<3, 3>{{0.01449275, -0.16425120, -0.04106280}, {0.02898550, 0.00483091, 0.00120772}, {0, 0, -0.125}});

    cout << "constexpr" << endl;

    constexpr MMatrix<2, 2> mat6_1 =
    {
        {1, 2},
        {3, 4}
    };

    test(mat6_1.read(0,1), 2);

    constexpr std::size_t i6_1 = mat6_1.get_rows();
    constexpr std::size_t i6_2 = mat6_1.get_cols();

    test(i6_1, 2ul);
    test(i6_2, 2ul);

    constexpr MMatrix<3, 1> mat6_2 = MMatrix<3, 1>({{6},{2},{1}}).insert(0, 0, MMatrix<2, 1>({{-1},{-2}}));

    test(mat6_2, MMatrix<3, 1>({{-1},{-2},{1}}));

    constexpr MMatrix<1, 2> mat6_3 = mat6_1.extract<1, 2>(0,0);

    test(mat6_3, MMatrix<1, 2>({{1, 2}}));

    constexpr MMatrix<2, 1> mat6_4 = mat6_3.transpose();

    test(mat6_4, MMatrix<2, 1>({{1},{2}}));

    constexpr MMatrix<3, 4> mat6_5 =
    {
        {5, -8, 6, 7},
        {-1, 5, 4, 3},
        {1, 3, 2, 10}
    };

    constexpr MMatrix<3, 4> mat6_6 = mat6_5.get_rre();

    test(mat6_6, MMatrix<3, 4>{{1, 0, 0, 5.01886792}, {0, 1, 0, 1.94339622}, {0, 0, 1, -0.42452830}});

    constexpr std::size_t i6_3 = mat6_6.get_rank();

    test(i6_3, 3ul);

    constexpr std::int64_t i6_4 = MMatrix<3, 3>({{1, 34, 0},{-6, 3, 2},{0, 0, -8}}).get_det();

    test(i6_4, -1656);

    std::cout << "Utility constructors" << std::endl;

    constexpr MMatrix<3, 4> mat7_1 = MMatrix<3, 4>::eye();

    test(mat7_1, MMatrix<3, 4>({{1, 0, 0, 0},{0, 1, 0, 0},{0, 0, 1, 0}}));

    constexpr MMatrix<4, 3> mat7_2 = MMatrix<4, 3>::eye();

    test(mat7_2, MMatrix<4, 3>({{1, 0, 0},{0, 1, 0},{0, 0, 1},{0, 0, 0}}));

    constexpr MMatrix<2, 1> mat7_3 = MMatrix<2, 1>::zeros();

    test(mat7_3, MMatrix<2, 1>({{0}, {0}}));

    constexpr MMatrix<2, 2> mat7_4 = MMatrix<2, 2>::ones();

    test(mat7_4, MMatrix<2, 2>({{1,1},{1,1}}));

    return 0;
}
