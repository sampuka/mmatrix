test: unittest.cpp MMatrix.hpp
	g++ -g -std=c++17 -Wall -Wextra -Wpedantic unittest.cpp -o test

.PHONY: install

install: MMatrix.hpp
	cp MMatrix.hpp /usr/local/include/MMatrix.hpp
