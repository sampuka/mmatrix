# Introduction
This repository offers the `MMatrix.hpp` C++ header file.
It contains a `MMatrix` class.
This library is written for personal usage but anyone can use it as they wish, though I would strongly appreciate sending me a message if you decide to use it.  

# Usage
Download the `MMatrix.hpp` file and add it to your project

# Documentation
Each value in a `MMatrix` is of `double` type.
Everything is 0-indexed!
Many of the functionalities returns a new `MMatrix` instead of editing its operands.
An uninitialized `MMatrix` will contain zeroes.
A `MMatrix` can be initialized or set in the following way, using `std::inilializer_list<std::inilializer_list<double>>`
```CPP
MMatrix<3, 3> mat =
    {
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };
```

An identity matrix, a matrix filled with zeros and a matrix filled with ones can be created using static member functions `eye()`, `zeros()` and `ones()`.
For non-square matrices, the main diagonal is filled with ones for an identity matrix.
```CPP
MMatrix<3, 4> I = MMatrix<3, 4>::eye();
MMatrix<2, 2> Z = MMatrix<2, 2>::zeros();
MMatrix<1, 5> O = MMatrix<1, 5>::ones();
```

Individual values in a `MMatrix` can be read or written to with `at()`. This edits the matrix.
Individual values in a `MMatrix` can be const read with `read()`.
A `MMatrix` can be printed to `stdout` with `cout` with `operator<<`.

Two `MMatrix` can be added element-wise if they're the same size with `operator+`.
A `MMatrix` can be element-wise multiplied with a float with `operator*`.
Two `MMatrix` can be matrix-multiplied if the sizes are correct with `operator*`.
Two `MMatrix` can be compared with `operator==`.

A `MMatrix` can be transposed with `transpose()`.
A reduced row echelon form of a `MMatrix` can be found with `get_rre()`.
The rank of a `MMatrix` can be found with `get_rank()`.
The determinant of a square `MMatrix` can be found with `get_det()`.
The Matrix of Minors of a `MMatrix` can be found with `get_mom()`.
The inverse of a `MMatrix` can be found with `get_inv()`.

A smaller or equal-to sized `MMatrix` can be extracted or inserted into another `MMatrix` with `extract()` and `insert()`. This returns a new `MMatrix` instead of editing.
