#ifndef MMATRIX_H_
#define MMATRIX_H_

#include <array>
#include <cstdint>
#include <initializer_list>
#include <iostream>
#include <string>
#include <iomanip>
#include <stdexcept>
#include <cmath>

template<std::size_t m, std::size_t n>
class MMatrix
{
public:
    constexpr MMatrix();
    constexpr MMatrix(const std::initializer_list<const std::initializer_list<double>> &list);

    constexpr static MMatrix<m, n> eye();
    constexpr static MMatrix<m, n> zeros();
    constexpr static MMatrix<m, n> ones();

    constexpr std::size_t get_rows() const {return rows;}
    constexpr std::size_t get_cols() const {return cols;}
    constexpr double read(std::size_t y, std::size_t x) const;
    constexpr double& at(std::size_t y, std::size_t x);

    template<std::size_t h, std::size_t w>
    constexpr MMatrix<h, w> extract(const std::size_t y, const std::size_t x) const;

    template<std::size_t h, std::size_t w>
    constexpr MMatrix<m, n> insert(const std::size_t y, const std::size_t x, const MMatrix<h, w> &mat);

    constexpr MMatrix<n, m> transpose() const;
    constexpr MMatrix<m, n> get_rre() const; //Reduced row echelon
    constexpr std::size_t get_rank() const; //Matrix rank
    constexpr double get_det() const; //Determinant
    constexpr MMatrix<m, n> get_mom() const; //Matrix of minors
    constexpr MMatrix<m, m> get_inv() const; //Inverse

    constexpr MMatrix& operator=(const std::initializer_list<std::initializer_list<double>> &list);
    constexpr MMatrix& operator=(const MMatrix<m, n> &mat);

private:
    const std::size_t rows = m;
    const std::size_t cols = n;
    std::array<std::array<double, n>, m> data;
};

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n>::MMatrix() : data{}
{
    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
            at(i, j) = 0;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n>::MMatrix(const std::initializer_list<const std::initializer_list<double>> &list) : data{}
{
    if (m != list.size())
        throw std::runtime_error("Initializer list ill-formed");

    for (const auto& row : list)
        if (n != row.size())
            throw std::runtime_error("Initializer list ill-formed");

    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
            data[i][j] = list.begin()[i].begin()[j];
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> MMatrix<m, n>::eye()
{
    MMatrix<m, n> mat;

    for (std::size_t i = 0; i < std::min(n, m); i++)
        mat.at(i,i) = 1;

    return mat;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> MMatrix<m, n>::zeros()
{
    return MMatrix<m, n>();
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> MMatrix<m, n>::ones()
{
    MMatrix<m, n> mat;

    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
            mat.at(i,j) = 1;

    return mat;
}

template<std::size_t m, std::size_t n>
constexpr double MMatrix<m, n>::read(std::size_t y, std::size_t x) const
{
    if ((y >= m) || (x >= n))
        throw std::runtime_error("Out of bounds in method read()");
    else
        return data[y][x];
}

template<std::size_t m, std::size_t n>
constexpr double& MMatrix<m, n>::at(std::size_t y, std::size_t x)
{
    if ((y >= m) || (x >= n))
        throw std::runtime_error("Out of bounds in method at()");
    else
        return data[y][x];
}

template<std::size_t m, std::size_t n>
template<std::size_t h, std::size_t w>
constexpr MMatrix<h, w> MMatrix<m, n>::extract(const std::size_t y, const std::size_t x) const
{
    if ((h+y > m) || (w+x > n))
        throw std::runtime_error("Out of bounds extract");

    MMatrix<h, w> mat;
    for (std::size_t i = 0; i < h; i++)
        for (std::size_t j = 0; j < w; j++)
            mat.at(i, j) = read(y+i, x+j);

    return mat;
}

template<std::size_t m, std::size_t n>
template<std::size_t h, std::size_t w>
constexpr MMatrix<m, n> MMatrix<m, n>::insert(const std::size_t y, const std::size_t x, const MMatrix<h, w> &mat)
{
    if ((h+y > m) || (w+x > n))
        throw std::runtime_error("Out of bounds insert");

    MMatrix<m, n> new_mat = *this;

    for (std::size_t i = 0; i < h; i++)
        for (std::size_t j = 0; j < w; j++)
            new_mat.at(y+i, x+j) = mat.read(i,j);

    return new_mat;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<n, m> MMatrix<m, n>::transpose() const
{
    MMatrix<n, m> res;

    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
            res.at(j, i) = read(i, j);

    return res;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> MMatrix<m, n>::get_rre() const
{
    MMatrix<m, n> mat = *this;

    for (std::size_t i = 0; i < std::min(m, n); i++)
    {
        //Find best row to swap to

        std::size_t best_row = -1;
        double best_row_value = -1;

        for (std::size_t j = i; j < m; j++)
        {
            if (std::abs(mat.read(j,i)) > best_row_value)
            {
                best_row = j;
                best_row_value = std::abs(mat.read(j,i));
            }
        }

        MMatrix<1, n> tmp = mat.extract<1, n>(i,0);

        mat = mat.insert(i, 0, mat.extract<1, n>(best_row, 0));
        mat = mat.insert(best_row, 0, tmp);

        if (mat.read(i, i) != 0)
        {
            mat = mat.insert(i, 0, mat.extract<1, n>(i, 0)/(mat.read(i, i)));

            for (std::size_t j = 0; j < m; j++)
            {
                if (j != i)
                {
                    mat = mat.insert(j, 0,
                            mat.extract<1, n>(j, 0)-
                            mat.read(j, i)*
                            mat.extract<1, n>(i, 0));
                }
            }
        }
    }

    return mat;
}

template<std::size_t m, std::size_t n>
constexpr std::size_t MMatrix<m, n>::get_rank() const
{
    MMatrix<m, n> rre = get_rre();

    std::size_t rank = 0;

    for (std::size_t i = 0; i < m; i++)
    {
        bool found = false;
        for (std::size_t j = 0; j < n; j++)
            found |= (rre.read(i, j) != 0);

        if (found)
            rank++;
    }

    return rank;
}

template<>
constexpr double MMatrix<1, 1>::get_det() const
{
    return read(0,0);
}

template<>
constexpr double MMatrix<2, 2>::get_det() const
{
    return read(0,0)*read(1,1)-read(1,0)*read(0,1);
}

//IMPORTANT: Is it possible to only define this for square matrixes?
template<std::size_t m, std::size_t n>
constexpr double MMatrix<m, n>::get_det() const
{
    double res = 0;

    if (m != n)
    {
        throw std::runtime_error("The determinant is not defined for a non-square matrix");
    }
    else
    {
        MMatrix<m-1, m-1> submat;
        for (std::size_t i = 0; i < m; i++)
        {
            for (std::size_t j = 0; j < m-1; j++)
            {
/*
                if constexpr (j >= i)
                    submat = submat.insert(j, 0, extract<1, m-1>(j+1, 1));
                else
                    submat = submat.insert(j, 0, extract<1, m-1>(j, 1));
*/
                submat = submat.insert(j, 0, extract<1, m-1>(j + (j>=i ? 1 : 0), 1));
            }

            res += pow(-1, i) * read(i, 0) * submat.get_det();
        }
    }

    return res;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> MMatrix<m, n>::get_mom() const
{
    MMatrix<m, n> res;

    for (std::size_t i = 0; i < m; i++)
    {
        for (std::size_t j = 0; j < n; j++)
        {
            MMatrix<m-1, n-1> submat;

            for (std::size_t a = 0; a < m-1; a++)
            {
                for (std::size_t b = 0; b < n-1; b++)
                {
                    if (a >= i)
                        if (b >= j)
                            submat.at(a, b) = read(a+1, b+1);
                        else
                            submat.at(a, b) = read(a+1, b);
                    else
                        if (b >= j)
                            submat.at(a, b) = read(a, b+1);
                        else
                            submat.at(a, b) = read(a, b);
                }
            }

            res.at(i, j) = submat.get_det();
        }
    }

    return res;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, m> MMatrix<m, n>::get_inv() const
{
    MMatrix<m, m> res;

    if (m != n)
    {
        throw std::runtime_error("The inverse is not defined for non-square matrixes");
    }
    else
    {
        res = get_mom();

        for (std::size_t i = 0; i < m; i++)
            for (std::size_t j = 0; j < n; j++)
                res.at(i, j) *= pow(-1, i+j);

        res = res.transpose() / get_det();
    }

    return res;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n>& MMatrix<m, n>::operator=(const std::initializer_list<std::initializer_list<double>> &list)
{
    if (m != list.size())
        throw std::runtime_error("Initializer list ill-formed");

    for (const auto& row : list)
        if (n != row.size())
            throw std::runtime_error("Initializer list ill-formed");

    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
            at(i, j) = list.begin()[i].begin()[j];

    return *this;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n>& MMatrix<m, n>::operator=(const MMatrix<m, n> &mat)
{
    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
            at(i, j) = mat.read(i, j);

    return *this;
}

template<std::size_t m, std::size_t n>
constexpr bool operator==(const MMatrix<m, n> &mat1, const MMatrix<m, n> &mat2)
{
    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
        {
            if ((!std::isfinite(mat1.read(i, j))) || (!std::isfinite(mat2.read(i ,j))))
                return false;

            if (abs(mat1.read(i, j) - mat2.read(i, j)) > 0.00001)
                return false;
        }

    return true;
}

template<std::size_t m, std::size_t n>
constexpr bool operator!=(const MMatrix<m, n> &mat1, const MMatrix<m, n> &mat2)
{
    return !(mat1 == mat2);
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> operator+(const MMatrix<m, n> &mat1, const MMatrix<m, n> &mat2)
{
    MMatrix<m, n> res;

    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
            res.at(i, j) = mat1.read(i, j) + mat2.read(i, j);

    return res;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> operator-(const MMatrix<m, n> &mat1, const MMatrix<m, n> &mat2)
{
    MMatrix<m, n> res;
    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < n; j++)
            res.at(i, j) = mat1.read(i, j) - mat2.read(i, j);

    return res;
}

template<std::size_t m, std::size_t n, std::size_t p>
constexpr MMatrix<m, p> operator*(const MMatrix<m, n> mat1, const MMatrix<n, p> &mat2)
{
    MMatrix<m, p> res;

    for (std::size_t i = 0; i < m; i++)
        for (std::size_t j = 0; j < p; j++)
            for (std::size_t r = 0; r < n; r++)
                res.at(i, j) += mat1.read(i, r) * mat2.read(r, j);

    return res;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> operator*(const MMatrix<m, n> mat, double num)
{
    MMatrix<m, n> res = mat;
    for (std::size_t i = 0; i < m; i++)
    {
        for (std::size_t j = 0; j < n; j++)
        {
            res.at(i, j) *= num;
        }
    }

    return res;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> operator/(const MMatrix<m, n> mat, double num)
{
    MMatrix<m, n> res = mat;
    for (std::size_t i = 0; i < m; i++)
    {
        for (std::size_t j = 0; j < n; j++)
        {
            res.at(i, j) /= num;
        }
    }

    return res;
}

template<std::size_t m, std::size_t n>
constexpr MMatrix<m, n> operator*(double num, const MMatrix<m, n> mat)
{
    return mat * num;
}

template<std::size_t m, std::size_t n>
std::ostream& operator<<(std::ostream &os, const MMatrix<m, n> &mat)
{
    os << "{\n";

    for (std::size_t i = 0; i < m; i++)
    {
        os << "  {";

        for (std::size_t j = 0; j < n; j++)
        {
            os << std::to_string(mat.read(i, j));
            if (j != n-1)
                std::cout << ", ";
        }

        os << '}';
        if (i != m-1)
            os << ',';
        os << '\n';
    }

    os << '}';

    return os;
}

#endif
